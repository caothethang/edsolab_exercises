package com.example;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
    private String name;
    private String artist;
    private ArrayList<Song> songs;
    
    public Album(String name, String artist) {
        this.name = name;
        this.artist = artist;
        this.songs = new ArrayList<Song>();
    }
    
    public boolean addSong(String title, double duration){
        if (this.findSong(title) == null){
            this.songs.add(new Song(title,duration));
            return true;
        }
        return false;
    }
    
    private Song findSong(String title){
        for(Song s : songs){
            if(s.getTitle().equals(title)){
                return s;
            }
        }
        return null;
    }
    
    public boolean addToPlayList(int trackNumber , LinkedList<Song> linkedList){
        if(trackNumber<1 || trackNumber > this.songs.size()-1){
            return false;
        }
        linkedList.add(this.songs.get(trackNumber-1));
        return true;
    }
    public boolean addToPlayList(String title, LinkedList<Song> list){
        if(this.findSong(title) == null){
            return false;
        }
        list.add(this.findSong(title));
        return true;
    }
    
}
