package com.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();         // Số lượng phần tử được nhập từ bàn phím
        sc.nextLine();
        double[] arr = createArray(N);
        System.out.println("Mảng vừa tạo: ");
        for (int i = 0; i < N; i++) {
            System.out.println(arr[i]);
        }
        System.out.println("Các ptu < 50");
        arr = removeElementLessThanFifty(arr);
        System.out.println("Sau khi xoá,số phần tử còn lại: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
    
    //Tạo mảng
    public static double[] createArray(int num) {
        double[] arr = new double[num];
        for (int i = 0; i < num; i++) {
            double random = Math.random() * 100;
            arr[i] = random;
        }
        return arr;
    }
    
    //xoá phần tử ra khỏi mảng
    public static double[] removeElementLessThanFifty(double[] arr) {
        List<Double> arrList = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] >= 50) {
                arrList.add(arr[i]);
            } else {
                System.out.println("Phần tử nhỏ hơn 50: " + arr[i]);
            }
        }
        arr = new double[arrList.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = arrList.get(i);
        }
        arrList.clear();
        return arr;
    }
}
